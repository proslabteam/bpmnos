collaboration( {noAction}
pool( "Customers" , 
proc( {emptyAction}
startRcv(enabled , "SequenceFlow_3" . 0 , "Travel Offer" .msg 0) | 
end( disabled , "sid-59E37E56-4C04-42F5-B4D7-B177613DC031" . 0 ) | 
end( disabled , "sid-D2CFC067-9F8B-4FDA-88F0-72FCD56B07B7" . 0 ) | 
task( disabled , "SequenceFlow_3" . 0 , "sid-D574BCCB-B07A-4960-860B-D3ADD9455A5C" . 0 , "Check Travel Offer") | 
taskSnd( disabled , "sid-AB94954E-DD45-4888-9556-602A386636DB" . 0 , "sid-D2CFC067-9F8B-4FDA-88F0-72FCD56B07B7" . 0 , "MessageFlow_1" .msg 0 , "Reject Offer") | 
taskSnd( disabled , "sid-7666477C-8B98-4E15-9547-7215DF0DA509" . 0 , "sid-CF2C3E07-D9C9-4299-A9AC-4012411A1279" . 0 , "Booking Request" .msg 0 , "Book Travel") | 
taskSnd( disabled , "sid-6E56D5A7-74CE-42E9-8659-714B13DB09A3" . 0 , "sid-2339668D-E305-411B-AD12-B64201A53BF6" . 0 , "Payment" .msg 0 , "Pay Travel") | 
interRcv( disabled , "sid-CF2C3E07-D9C9-4299-A9AC-4012411A1279" . 0 , "sid-6E56D5A7-74CE-42E9-8659-714B13DB09A3" . 0 , "Booking Confirmation" .msg 0 ) | 
interRcv( disabled , "sid-2339668D-E305-411B-AD12-B64201A53BF6" . 0 , "sid-59E37E56-4C04-42F5-B4D7-B177613DC031" . 0 , "Payment Confirmation" .msg 0 ) | 
xorSplit( "sid-D574BCCB-B07A-4960-860B-D3ADD9455A5C" . 0 , edges( "sid-AB94954E-DD45-4888-9556-602A386636DB" . 0 and "sid-7666477C-8B98-4E15-9547-7215DF0DA509" . 0 )  ) | emptyProcElements ) , 
in: "Travel Offer" .msg 0 andmsg "Booking Confirmation" .msg 0 andmsg "Payment Confirmation" .msg 0 andmsg  emptyMsgSet , out: "MessageFlow_1" .msg 0 andmsg "Booking Request" .msg 0 andmsg "Payment" .msg 0 andmsg  emptyMsgSet ) | 
pool( "Travel Agency" , 
proc( {emptyAction}
start( enabled , "sid-4A590D9A-8375-4856-9DDD-2841A06B3098" . 0 ) | 
end( disabled , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 ) | 
end( disabled , "sid-6375C9F1-0C10-44BC-BC94-A345A86A61DF" . 0 ) | 
taskSnd( disabled , "sid-4A590D9A-8375-4856-9DDD-2841A06B3098" . 0 , "sid-6F278A13-0DB0-4DF5-AE85-0B365E914FA5" . 0 , "Travel Offer" .msg 0 , "Make Travel Offer") | 
taskSnd( disabled , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "sid-C306BC36-14C7-4E31-A7C4-99CA513D4EA8" . 0 , "Booking Confirmation" .msg 0 , "Confirmation Booking") | 
taskSnd( disabled , "sid-79E4FCCE-1996-4587-B3B9-75F22E1DB033" . 0 , "sid-6375C9F1-0C10-44BC-BC94-A345A86A61DF" . 0 , "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 , "Order Ticket") | 
interRcv( disabled , "sid-5B28E377-574A-4D9F-B8A2-7ADFF87BD2D2" . 0 , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "Booking Request" .msg 0 ) | 
interRcv( disabled , "sid-C306BC36-14C7-4E31-A7C4-99CA513D4EA8" . 0 , "sid-79E4FCCE-1996-4587-B3B9-75F22E1DB033" . 0 , "Payment" .msg 0 ) | 
interRcv( disabled , "sid-6A236614-BEBC-4E47-993A-CB660C528C04" . 0 , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 , "MessageFlow_1" .msg 0 ) | 
eventSplit( "sid-6F278A13-0DB0-4DF5-AE85-0B365E914FA5" . 0 , eventRcvSplit( eventInterRcv( disabled , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "Booking Request" .msg 0 )  ^ eventInterRcv( disabled , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 , "MessageFlow_1" .msg 0 )  )  ) | emptyProcElements ) , 
in: "Booking Request" .msg 0 andmsg "Payment" .msg 0 andmsg "MessageFlow_1" .msg 0 andmsg "Booking Request" .msg 0 andmsg "MessageFlow_1" .msg 0 andmsg  emptyMsgSet , out: "Travel Offer" .msg 0 andmsg "Booking Confirmation" .msg 0 andmsg "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 andmsg  emptyMsgSet ) | 
pool( "Airline" , 
proc( {emptyAction}
startRcv(enabled , "sid-F969BBC6-3367-4768-9180-44C040A3DEB7" . 0 , "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0) | 
end( disabled , "sid-E3D5ED09-D303-4195-8E27-516CF4D132AF" . 0 ) | 
end( disabled , "sid-A8BA8C4E-CC18-467C-982D-7A9F5DFC0B4C" . 0 ) | 
task( disabled , "sid-F969BBC6-3367-4768-9180-44C040A3DEB7" . 0 , "sid-8D5C98CA-A20A-4264-93CF-ECDF6224901C" . 0 , "Handle Payment") | 
taskSnd( disabled , "sid-D7D076AB-E6C3-428E-9838-3BB08B7AD7F2" . 0 , "sid-A8BA8C4E-CC18-467C-982D-7A9F5DFC0B4C" . 0 , "Payment Confirmation" .msg 0 , "Confirm Payment") | 
xorSplit( "sid-8D5C98CA-A20A-4264-93CF-ECDF6224901C" . 0 , edges( "sid-E3D5ED09-D303-4195-8E27-516CF4D132AF" . 0 and "sid-D7D076AB-E6C3-428E-9838-3BB08B7AD7F2" . 0 )  ) | emptyProcElements ) , 
in: "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 andmsg  emptyMsgSet , out: "Payment Confirmation" .msg 0 andmsg  emptyMsgSet ) | emptyPool )collaboration( {noAction}
pool( "Customers" , 
proc( {emptyAction}
startRcv(enabled , "SequenceFlow_3" . 0 , "Travel Offer" .msg 0) | 
end( disabled , "sid-59E37E56-4C04-42F5-B4D7-B177613DC031" . 0 ) | 
end( disabled , "sid-D2CFC067-9F8B-4FDA-88F0-72FCD56B07B7" . 0 ) | 
task( disabled , "SequenceFlow_3" . 0 , "sid-D574BCCB-B07A-4960-860B-D3ADD9455A5C" . 0 , "Check Travel Offer") | 
taskSnd( disabled , "sid-AB94954E-DD45-4888-9556-602A386636DB" . 0 , "sid-D2CFC067-9F8B-4FDA-88F0-72FCD56B07B7" . 0 , "MessageFlow_1" .msg 0 , "Reject Offer") | 
taskSnd( disabled , "sid-7666477C-8B98-4E15-9547-7215DF0DA509" . 0 , "sid-CF2C3E07-D9C9-4299-A9AC-4012411A1279" . 0 , "Booking Request" .msg 0 , "Book Travel") | 
taskSnd( disabled , "sid-6E56D5A7-74CE-42E9-8659-714B13DB09A3" . 0 , "sid-2339668D-E305-411B-AD12-B64201A53BF6" . 0 , "Payment" .msg 0 , "Pay Travel") | 
interRcv( disabled , "sid-CF2C3E07-D9C9-4299-A9AC-4012411A1279" . 0 , "sid-6E56D5A7-74CE-42E9-8659-714B13DB09A3" . 0 , "Booking Confirmation" .msg 0 ) | 
interRcv( disabled , "sid-2339668D-E305-411B-AD12-B64201A53BF6" . 0 , "sid-59E37E56-4C04-42F5-B4D7-B177613DC031" . 0 , "Payment Confirmation" .msg 0 ) | 
xorSplit( "sid-D574BCCB-B07A-4960-860B-D3ADD9455A5C" . 0 , edges( "sid-AB94954E-DD45-4888-9556-602A386636DB" . 0 and "sid-7666477C-8B98-4E15-9547-7215DF0DA509" . 0 )  ) | emptyProcElements ) , 
in: "Travel Offer" .msg 0 andmsg "Booking Confirmation" .msg 0 andmsg "Payment Confirmation" .msg 0 andmsg  emptyMsgSet , out: "MessageFlow_1" .msg 0 andmsg "Booking Request" .msg 0 andmsg "Payment" .msg 0 andmsg  emptyMsgSet ) | 
pool( "Travel Agency" , 
proc( {emptyAction}
start( enabled , "sid-4A590D9A-8375-4856-9DDD-2841A06B3098" . 0 ) | 
end( disabled , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 ) | 
end( disabled , "sid-6375C9F1-0C10-44BC-BC94-A345A86A61DF" . 0 ) | 
taskSnd( disabled , "sid-4A590D9A-8375-4856-9DDD-2841A06B3098" . 0 , "sid-6F278A13-0DB0-4DF5-AE85-0B365E914FA5" . 0 , "Travel Offer" .msg 0 , "Make Travel Offer") | 
taskSnd( disabled , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "sid-C306BC36-14C7-4E31-A7C4-99CA513D4EA8" . 0 , "Booking Confirmation" .msg 0 , "Confirmation Booking") | 
taskSnd( disabled , "sid-79E4FCCE-1996-4587-B3B9-75F22E1DB033" . 0 , "sid-6375C9F1-0C10-44BC-BC94-A345A86A61DF" . 0 , "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 , "Order Ticket") | 
interRcv( disabled , "sid-5B28E377-574A-4D9F-B8A2-7ADFF87BD2D2" . 0 , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "Booking Request" .msg 0 ) | 
interRcv( disabled , "sid-C306BC36-14C7-4E31-A7C4-99CA513D4EA8" . 0 , "sid-79E4FCCE-1996-4587-B3B9-75F22E1DB033" . 0 , "Payment" .msg 0 ) | 
interRcv( disabled , "sid-6A236614-BEBC-4E47-993A-CB660C528C04" . 0 , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 , "MessageFlow_1" .msg 0 ) | 
eventSplit( "sid-6F278A13-0DB0-4DF5-AE85-0B365E914FA5" . 0 , eventRcvSplit( eventInterRcv( disabled , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "Booking Request" .msg 0 )  ^ eventInterRcv( disabled , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 , "MessageFlow_1" .msg 0 )  )  ) | emptyProcElements ) , 
in: "Booking Request" .msg 0 andmsg "Payment" .msg 0 andmsg "MessageFlow_1" .msg 0 andmsg "Booking Request" .msg 0 andmsg "MessageFlow_1" .msg 0 andmsg  emptyMsgSet , out: "Travel Offer" .msg 0 andmsg "Booking Confirmation" .msg 0 andmsg "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 andmsg  emptyMsgSet ) | 
pool( "Airline" , 
proc( {emptyAction}
startRcv(enabled , "sid-F969BBC6-3367-4768-9180-44C040A3DEB7" . 0 , "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0) | 
end( disabled , "sid-E3D5ED09-D303-4195-8E27-516CF4D132AF" . 0 ) | 
end( disabled , "sid-A8BA8C4E-CC18-467C-982D-7A9F5DFC0B4C" . 0 ) | 
task( disabled , "sid-F969BBC6-3367-4768-9180-44C040A3DEB7" . 0 , "sid-8D5C98CA-A20A-4264-93CF-ECDF6224901C" . 0 , "Handle Payment") | 
taskSnd( disabled , "sid-D7D076AB-E6C3-428E-9838-3BB08B7AD7F2" . 0 , "sid-A8BA8C4E-CC18-467C-982D-7A9F5DFC0B4C" . 0 , "Payment Confirmation" .msg 0 , "Confirm Payment") | 
xorSplit( "sid-8D5C98CA-A20A-4264-93CF-ECDF6224901C" . 0 , edges( "sid-E3D5ED09-D303-4195-8E27-516CF4D132AF" . 0 and "sid-D7D076AB-E6C3-428E-9838-3BB08B7AD7F2" . 0 )  ) | emptyProcElements ) , 
in: "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 andmsg  emptyMsgSet , out: "Payment Confirmation" .msg 0 andmsg  emptyMsgSet ) | emptyPool )collaboration( {noAction}
pool( "Customers" , 
proc( {emptyAction}
startRcv(enabled , "SequenceFlow_3" . 0 , "Travel Offer" .msg 0) | 
end( disabled , "sid-59E37E56-4C04-42F5-B4D7-B177613DC031" . 0 ) | 
end( disabled , "sid-D2CFC067-9F8B-4FDA-88F0-72FCD56B07B7" . 0 ) | 
task( disabled , "SequenceFlow_3" . 0 , "sid-D574BCCB-B07A-4960-860B-D3ADD9455A5C" . 0 , "Check Travel Offer") | 
taskSnd( disabled , "sid-AB94954E-DD45-4888-9556-602A386636DB" . 0 , "sid-D2CFC067-9F8B-4FDA-88F0-72FCD56B07B7" . 0 , "MessageFlow_1" .msg 0 , "Reject Offer") | 
taskSnd( disabled , "sid-7666477C-8B98-4E15-9547-7215DF0DA509" . 0 , "sid-CF2C3E07-D9C9-4299-A9AC-4012411A1279" . 0 , "Booking Request" .msg 0 , "Book Travel") | 
taskSnd( disabled , "sid-6E56D5A7-74CE-42E9-8659-714B13DB09A3" . 0 , "sid-2339668D-E305-411B-AD12-B64201A53BF6" . 0 , "Payment" .msg 0 , "Pay Travel") | 
interRcv( disabled , "sid-CF2C3E07-D9C9-4299-A9AC-4012411A1279" . 0 , "sid-6E56D5A7-74CE-42E9-8659-714B13DB09A3" . 0 , "Booking Confirmation" .msg 0 ) | 
interRcv( disabled , "sid-2339668D-E305-411B-AD12-B64201A53BF6" . 0 , "sid-59E37E56-4C04-42F5-B4D7-B177613DC031" . 0 , "Payment Confirmation" .msg 0 ) | 
xorSplit( "sid-D574BCCB-B07A-4960-860B-D3ADD9455A5C" . 0 , edges( "sid-AB94954E-DD45-4888-9556-602A386636DB" . 0 and "sid-7666477C-8B98-4E15-9547-7215DF0DA509" . 0 )  ) | emptyProcElements ) , 
in: "Travel Offer" .msg 0 andmsg "Booking Confirmation" .msg 0 andmsg "Payment Confirmation" .msg 0 andmsg  emptyMsgSet , out: "MessageFlow_1" .msg 0 andmsg "Booking Request" .msg 0 andmsg "Payment" .msg 0 andmsg  emptyMsgSet ) | 
pool( "Travel Agency" , 
proc( {emptyAction}
start( enabled , "sid-4A590D9A-8375-4856-9DDD-2841A06B3098" . 0 ) | 
end( disabled , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 ) | 
end( disabled , "sid-6375C9F1-0C10-44BC-BC94-A345A86A61DF" . 0 ) | 
taskSnd( disabled , "sid-4A590D9A-8375-4856-9DDD-2841A06B3098" . 0 , "sid-6F278A13-0DB0-4DF5-AE85-0B365E914FA5" . 0 , "Travel Offer" .msg 0 , "Make Travel Offer") | 
taskSnd( disabled , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "sid-C306BC36-14C7-4E31-A7C4-99CA513D4EA8" . 0 , "Booking Confirmation" .msg 0 , "Confirmation Booking") | 
taskSnd( disabled , "sid-79E4FCCE-1996-4587-B3B9-75F22E1DB033" . 0 , "sid-6375C9F1-0C10-44BC-BC94-A345A86A61DF" . 0 , "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 , "Order Ticket") | 
interRcv( disabled , "sid-5B28E377-574A-4D9F-B8A2-7ADFF87BD2D2" . 0 , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "Booking Request" .msg 0 ) | 
interRcv( disabled , "sid-C306BC36-14C7-4E31-A7C4-99CA513D4EA8" . 0 , "sid-79E4FCCE-1996-4587-B3B9-75F22E1DB033" . 0 , "Payment" .msg 0 ) | 
interRcv( disabled , "sid-6A236614-BEBC-4E47-993A-CB660C528C04" . 0 , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 , "MessageFlow_1" .msg 0 ) | 
eventSplit( "sid-6F278A13-0DB0-4DF5-AE85-0B365E914FA5" . 0 , eventRcvSplit( eventInterRcv( disabled , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "Booking Request" .msg 0 )  ^ eventInterRcv( disabled , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 , "MessageFlow_1" .msg 0 )  )  ) | emptyProcElements ) , 
in: "Booking Request" .msg 0 andmsg "Payment" .msg 0 andmsg "MessageFlow_1" .msg 0 andmsg "Booking Request" .msg 0 andmsg "MessageFlow_1" .msg 0 andmsg  emptyMsgSet , out: "Travel Offer" .msg 0 andmsg "Booking Confirmation" .msg 0 andmsg "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 andmsg  emptyMsgSet ) | 
pool( "Airline" , 
proc( {emptyAction}
startRcv(enabled , "sid-F969BBC6-3367-4768-9180-44C040A3DEB7" . 0 , "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0) | 
end( disabled , "sid-E3D5ED09-D303-4195-8E27-516CF4D132AF" . 0 ) | 
end( disabled , "sid-A8BA8C4E-CC18-467C-982D-7A9F5DFC0B4C" . 0 ) | 
task( disabled , "sid-F969BBC6-3367-4768-9180-44C040A3DEB7" . 0 , "sid-8D5C98CA-A20A-4264-93CF-ECDF6224901C" . 0 , "Handle Payment") | 
taskSnd( disabled , "sid-D7D076AB-E6C3-428E-9838-3BB08B7AD7F2" . 0 , "sid-A8BA8C4E-CC18-467C-982D-7A9F5DFC0B4C" . 0 , "Payment Confirmation" .msg 0 , "Confirm Payment") | 
xorSplit( "sid-8D5C98CA-A20A-4264-93CF-ECDF6224901C" . 0 , edges( "sid-E3D5ED09-D303-4195-8E27-516CF4D132AF" . 0 and "sid-D7D076AB-E6C3-428E-9838-3BB08B7AD7F2" . 0 )  ) | emptyProcElements ) , 
in: "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 andmsg  emptyMsgSet , out: "Payment Confirmation" .msg 0 andmsg  emptyMsgSet ) | emptyPool )collaboration( {noAction}
pool( "Customers" , 
proc( {emptyAction}
startRcv(enabled , "SequenceFlow_3" . 0 , "Travel Offer" .msg 0) | 
end( disabled , "sid-59E37E56-4C04-42F5-B4D7-B177613DC031" . 0 ) | 
end( disabled , "sid-D2CFC067-9F8B-4FDA-88F0-72FCD56B07B7" . 0 ) | 
task( disabled , "SequenceFlow_3" . 0 , "sid-D574BCCB-B07A-4960-860B-D3ADD9455A5C" . 0 , "Check Travel Offer") | 
taskSnd( disabled , "sid-AB94954E-DD45-4888-9556-602A386636DB" . 0 , "sid-D2CFC067-9F8B-4FDA-88F0-72FCD56B07B7" . 0 , "MessageFlow_1" .msg 0 , "Reject Offer") | 
taskSnd( disabled , "sid-7666477C-8B98-4E15-9547-7215DF0DA509" . 0 , "sid-CF2C3E07-D9C9-4299-A9AC-4012411A1279" . 0 , "Booking Request" .msg 0 , "Book Travel") | 
taskSnd( disabled , "sid-6E56D5A7-74CE-42E9-8659-714B13DB09A3" . 0 , "sid-2339668D-E305-411B-AD12-B64201A53BF6" . 0 , "Payment" .msg 0 , "Pay Travel") | 
interRcv( disabled , "sid-CF2C3E07-D9C9-4299-A9AC-4012411A1279" . 0 , "sid-6E56D5A7-74CE-42E9-8659-714B13DB09A3" . 0 , "Booking Confirmation" .msg 0 ) | 
interRcv( disabled , "sid-2339668D-E305-411B-AD12-B64201A53BF6" . 0 , "sid-59E37E56-4C04-42F5-B4D7-B177613DC031" . 0 , "Payment Confirmation" .msg 0 ) | 
xorSplit( "sid-D574BCCB-B07A-4960-860B-D3ADD9455A5C" . 0 , edges( "sid-AB94954E-DD45-4888-9556-602A386636DB" . 0 and "sid-7666477C-8B98-4E15-9547-7215DF0DA509" . 0 )  ) | emptyProcElements ) , 
in: "Travel Offer" .msg 0 andmsg "Booking Confirmation" .msg 0 andmsg "Payment Confirmation" .msg 0 andmsg  emptyMsgSet , out: "MessageFlow_1" .msg 0 andmsg "Booking Request" .msg 0 andmsg "Payment" .msg 0 andmsg  emptyMsgSet ) | 
pool( "Travel Agency" , 
proc( {emptyAction}
start( enabled , "sid-4A590D9A-8375-4856-9DDD-2841A06B3098" . 0 ) | 
end( disabled , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 ) | 
end( disabled , "sid-6375C9F1-0C10-44BC-BC94-A345A86A61DF" . 0 ) | 
taskSnd( disabled , "sid-4A590D9A-8375-4856-9DDD-2841A06B3098" . 0 , "sid-6F278A13-0DB0-4DF5-AE85-0B365E914FA5" . 0 , "Travel Offer" .msg 0 , "Make Travel Offer") | 
taskSnd( disabled , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "sid-C306BC36-14C7-4E31-A7C4-99CA513D4EA8" . 0 , "Booking Confirmation" .msg 0 , "Confirmation Booking") | 
taskSnd( disabled , "sid-79E4FCCE-1996-4587-B3B9-75F22E1DB033" . 0 , "sid-6375C9F1-0C10-44BC-BC94-A345A86A61DF" . 0 , "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 , "Order Ticket") | 
interRcv( disabled , "sid-5B28E377-574A-4D9F-B8A2-7ADFF87BD2D2" . 0 , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "Booking Request" .msg 0 ) | 
interRcv( disabled , "sid-C306BC36-14C7-4E31-A7C4-99CA513D4EA8" . 0 , "sid-79E4FCCE-1996-4587-B3B9-75F22E1DB033" . 0 , "Payment" .msg 0 ) | 
interRcv( disabled , "sid-6A236614-BEBC-4E47-993A-CB660C528C04" . 0 , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 , "MessageFlow_1" .msg 0 ) | 
eventSplit( "sid-6F278A13-0DB0-4DF5-AE85-0B365E914FA5" . 0 , eventRcvSplit( eventInterRcv( disabled , "sid-8628363D-CC20-4BD6-8C3B-366CCAFCF563" . 0 , "Booking Request" .msg 0 )  ^ eventInterRcv( disabled , "sid-EEA3095F-3EC4-4831-ADEB-71CBF3C57F99" . 0 , "MessageFlow_1" .msg 0 )  )  ) | emptyProcElements ) , 
in: "Booking Request" .msg 0 andmsg "Payment" .msg 0 andmsg "MessageFlow_1" .msg 0 andmsg "Booking Request" .msg 0 andmsg "MessageFlow_1" .msg 0 andmsg  emptyMsgSet , out: "Travel Offer" .msg 0 andmsg "Booking Confirmation" .msg 0 andmsg "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 andmsg  emptyMsgSet ) | 
pool( "Airline" , 
proc( {emptyAction}
startRcv(enabled , "sid-F969BBC6-3367-4768-9180-44C040A3DEB7" . 0 , "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0) | 
end( disabled , "sid-E3D5ED09-D303-4195-8E27-516CF4D132AF" . 0 ) | 
end( disabled , "sid-A8BA8C4E-CC18-467C-982D-7A9F5DFC0B4C" . 0 ) | 
task( disabled , "sid-F969BBC6-3367-4768-9180-44C040A3DEB7" . 0 , "sid-8D5C98CA-A20A-4264-93CF-ECDF6224901C" . 0 , "Handle Payment") | 
taskSnd( disabled , "sid-D7D076AB-E6C3-428E-9838-3BB08B7AD7F2" . 0 , "sid-A8BA8C4E-CC18-467C-982D-7A9F5DFC0B4C" . 0 , "Payment Confirmation" .msg 0 , "Confirm Payment") | 
xorSplit( "sid-8D5C98CA-A20A-4264-93CF-ECDF6224901C" . 0 , edges( "sid-E3D5ED09-D303-4195-8E27-516CF4D132AF" . 0 and "sid-D7D076AB-E6C3-428E-9838-3BB08B7AD7F2" . 0 )  ) | emptyProcElements ) , 
in: "sid-FA64B442-03BD-4078-9F82-8ED85B058FBE" .msg 0 andmsg  emptyMsgSet , out: "Payment Confirmation" .msg 0 andmsg  emptyMsgSet ) | emptyPool )