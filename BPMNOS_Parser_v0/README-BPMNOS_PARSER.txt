BPMNOS_Parser REQUIREMENTS

ELEMENT RESTRICTION
The Parser from .bpmn to the BPMNOS Syntax takes in input a .bpmn file with a core set of BPMN elements:

pool
start event
message start event
end event
message end event
terminate end event
task
sendtask
receivetask
intermidiate throw event
intermidiate catch event
xorsplit gateway
xorjoin gateway
andsplit gateway
andjoin gateway
orsplit gateway
event based gateway

If other elements that cannot be parsed are present in the .bpmn model, the parser sends an error message to the user listing the elements it could not parse.

WELL-DEFINED
We only consider specifications that are well-defined, in the sense that they comply with the following four syntactic constraints: 
i) distinct pools have different organisation names; 
ii) in a collaboration, for each message edge labelled by m outgoing from a pool, there exists only one corresponding message edge labelled by m incoming into another pool, and vice versa; 
iii) in a process, for each sequence edge labelled by e outgoing from a node, there exists only one corresponding sequence edge labelled by e incoming into another node, and vice versa; 
iv) for each incoming (resp. outgoing) message edge labelled by m at pool level, there exists only one corresponding incoming (resp. outgoing) message edge labelled by m at the level of the process within the pool, and vice versa. 
Each term of the language can be easily derived from a BPMN model whose only constraint is to have (organisation and message/sequence edge) unique names.
If the same label is present multiple times in the model, the parser send an error message to the user listing the label that has to be rectified.

HOW TO USE IT
Open a terminal, positionate into the parser directory and execute the following command:
java -jar BPMNOS_Parser.jar model_name.bpmn resulting_file_name

Together with the resulting file, including the model parsed into the BPMNOS syntax,
a log file with details about time of parsing and elements present in the model will be generate with the name BPMNOS_Parser_Log.txt.



